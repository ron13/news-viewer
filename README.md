# News viewer

Real-time news (Twitter) reader.
Now it's using search/tweets on each page load but it will be better to store tweets somewhere (database or elsewhere) to avoid Twitter rate limit.

## Installation

### Requirements

1. PHP 7.1.8 or later
2. Nginx or Apache web-server
2. PHP sockets extension

Or you can run it with just Docker.

### Run via Docker

The easiest way to run the app is via docker and docker-compose.
```
docker-compose run composer && docker-compose up
```

It will download all required dependencies and start all required services for you.
The app should be accessible in the browser as [http://localhost](http://localhost).

If you're using Windows and Docker toolbox - you should run
```
docker-machine ip
```
to find out your docker IP address and it will be your entry point for the app.

### Manual install

Clone app to your web-server root directory. Point to it in your terminal and install all dependencies via composer: 
```
composer install
```
Next, run the web socket process:
```
php command news:socket &
```
And run the News listener:
```
php command news:listen
```

You also need to configure your server to point to the `public` folder of the app.
If you use Nginx you can view configuration example in the `docker/nginx/default.conf` file.

### Configure

You should configure at least twitter credentials. Copy `.env.example` to `.env` if you didn't it before and update configuration:
```
# Twitter credentials
TWITTER_ACCESS_TOKEN=twitter_access_token
TWITTER_ACCESS_TOKEN_SECRET=twitter_access_token_secret
TWITTER_CONSUMER_KEY=twitter_consumer_key
TWITTER_CONSUMER_SECRET=twitter_consumer_secret
```
