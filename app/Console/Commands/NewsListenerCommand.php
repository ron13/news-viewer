<?php

namespace App\Console\Commands;

use App\Console\CommandInterface;
use App\News\Reader;

class NewsListenerCommand implements CommandInterface
{
    private $reader;

    public function __construct()
    {
        try {
            $this->reader = Reader::create(config('news.default_news_provider'));
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->reader->watch();
    }
}