<?php

namespace App\Console\Commands;

use App\Console\CommandInterface;
use App\News\NewsSocket;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class NewsSocketCommand implements CommandInterface
{
    private $server;

    public function __construct()
    {
        $this->server   = IoServer::factory(new HttpServer(
            new WsServer(
                new NewsSocket()
            )
        ), config('news.port'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->server->run();
    }
}