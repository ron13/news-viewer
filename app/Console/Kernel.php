<?php

namespace App\Console;

use App\Console\Commands\NewsListenerCommand;
use App\Console\Commands\NewsSocketCommand;

class Kernel
{
    /**
     * The commands provided by application.
     *
     * @var array
     */
    protected $commands = [
        'news:listen'     => NewsListenerCommand::class,
        'news:socket'     => NewsSocketCommand::class,
    ];

    public function __construct($argv)
    {
        if (isset($argv[1]) && array_key_exists($argv[1], $this->commands)) {
            $command = new $this->commands[$argv[1]]();
            $command->handle();
        }
    }
}
