<?php
/**
 * Created by PhpStorm.
 * User: vitaliy
 * Date: 11/3/18
 * Time: 8:40 PM
 */

namespace App\Exceptions;


use Whoops\Handler\PrettyPageHandler;

class Handler extends PrettyPageHandler
{


    /**
     * @return int|null A handler may return nothing, or a Handler::HANDLE_* constant
     * @throws \Exception
     */
    public function handle()
    {
        if (config('app.debug')) {
            return parent::handle();
        } else {
            response()->view('error')->send();
            return Handler::QUIT;
        }
    }
}