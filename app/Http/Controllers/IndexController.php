<?php
/**
 * Date: 10/13/2018
 * Time: 9:53 PM
 */

namespace App\Http\Controllers;


use App\News\Readers\TwitterReader;
use App\News\Reader;

class IndexController extends Controller
{

    public function index()
    {
        $reader = Reader::create(config('news.default_news_provider'));
        return response()->view('index', [
            'news' => $reader->getMessages(),
        ]);
    }
}