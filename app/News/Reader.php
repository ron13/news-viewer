<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 10/27/2018
 * Time: 9:03 PM
 */

namespace App\News;


use App\Exceptions\NewsReaderException;

class Reader
{
    private $reader;

    public function __construct(ReaderInterface $reader)
    {
        $this->reader = $reader;
    }


    /**
     * Watch for changes
     *
     * @return mixed
     */
    public function watch()
    {
        $this->reader->watch();
    }

    /**
     * Get last items
     *
     * @return array
     */
    public function getMessages(): array
    {
        return $this->reader->getMessages();
    }

    /**
     * Create new reader
     *
     * @param string $reader
     * @return Reader
     * @throws \Exception
     */
    public static function create(string $reader): self
    {
        $readerClass = 'App\\News\\Readers\\' . ucfirst($reader) . 'Reader';
        if (class_exists($readerClass)) {
            return new self(new $readerClass);
        }
        throw new NewsReaderException("Unknown news reader {$readerClass}");
    }
}