<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 10/27/2018
 * Time: 9:04 PM
 */

namespace App\News;


interface ReaderInterface
{
    /**
     * Watch for changes
     *
     * @return mixed
     */
    public function watch();

    /**
     * Format message and encode to json
     * Should return json representation of:
     * [
     *      "message" => "message text",
     *      "date"    => "Y/m/d H:i",
     *      "id"      => "some ID",
     * ]
     *
     * @param $message
     * @return string
     */
    public function formatJsonMessage($message): string ;

    /**
     * Format message
     * Should return:
     * [
     *      "message" => "message text",
     *      "date"    => "Y/m/d H:i",
     *      "id"      => "some ID",
     * ]
     *
     * @param $message
     * @return array
     */
    public function formatMessage($message): array;

    /**
     * Get last items
     *
     * @return array
     */
    public function getMessages(): array;
}