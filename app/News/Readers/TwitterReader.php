<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 10/27/2018
 * Time: 10:24 PM
 */

namespace App\News\Readers;


use Abraham\TwitterOAuth\TwitterOAuth;
use App\Exceptions\NewsReaderException;
use App\News\ReaderInterface;
use Carbon\Carbon;
use Spatie\TwitterStreamingApi\PublicStream;
use WebSocket\Client;

class TwitterReader implements ReaderInterface
{

    /**
     * @param string
     */
    private $newsAddr;

    /**
     * @param integer
     */
    private $newsPort;

    /**
     * @param string
     */
    private $accessToken;

    /**
     * @param string
     */
    private $accessTokenSecret;

    /**
     * @param string
     */
    private $consumerKey;

    /**
     * @param string
     */
    private $consumerSecret;

    /**
     * @param string
     */
    private $search;


    public function __construct()
    {
        $this->accessToken       = config('twitter.access_token');
        $this->accessTokenSecret = config('twitter.access_token_secret');
        $this->consumerKey       = config('twitter.consumer_key');
        $this->consumerSecret    = config('twitter.consumer_secret');
        $this->search            = config('twitter.search');
        $this->newsAddr          = config('news.address');
        $this->newsPort          = config('news.port');
    }

    /**
     * Watch for changes
     *
     * @return mixed
     */
    public function watch()
    {
        $stream = PublicStream::create(
            $this->accessToken,
            $this->accessTokenSecret,
            $this->consumerKey,
            $this->consumerSecret
        );
        $client = new Client("ws://{$this->newsAddr}:{$this->newsPort}");
        $stream
            ->whenFrom([], function() {})
            ->whenTweets(null, function() {})
            ->whenHears($this->search, function(array $tweet) use ($client) {
                $client->send($this->formatJsonMessage($tweet));
            })
            ->startListening();
    }

    /**
     * Format message
     * Should return:
     * [
     *      "message" => "message text",
     *      "date"    => "Y/m/d H:i",
     *      "id"      => "some ID",
     * ]
     *
     * @param $message
     * @return array
     */
    public function formatMessage($message): array
    {
        return [
            'message' => $message->text,
            'date'    => Carbon::parse($message->created_at)->format('Y/m/d H:i'),
            'id'      => $message->id,
        ];
    }

    /**
     * Format message and encode to json
     * Should return json representation of:
     * [
     *      "message" => "message text",
     *      "date"    => "Y/m/d H:i",
     *      "id"      => "some ID",
     * ]
     *
     * @param $message
     * @return string
     */
    public function formatJsonMessage($message): string
    {
        return json_encode($this->formatMessage((object) $message));
    }

    /**
     * Get last items
     *
     * @return array
     * @throws NewsReaderException
     */
    public function getMessages(): array
    {
        $connection = new TwitterOAuth(
            $this->consumerKey,
            $this->consumerSecret,
            $this->accessToken,
            $this->accessTokenSecret
        );
        $result = $connection->get("search/tweets", [
            "q"     => config('twitter.listen_for', $this->search),
            "count" => config('news.max'),
        ]);
        if ($result->errors) {
            throw new NewsReaderException("Twitter API error: {$result->errors[0]->message}", $result->errors[0]->code);
        }
        $items = [];
        foreach ($result->statuses as $item) {
            $items[] = $this->formatMessage($item);
        }
        return $items;
    }
}