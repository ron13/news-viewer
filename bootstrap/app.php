<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../routes/web.php';
require_once __DIR__.'/../main/helpers/app.php';
$app = \Main\App::getInstance();
return $app;
