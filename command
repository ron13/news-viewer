#!/usr/bin/env php
<?php
require_once __DIR__.'/vendor/autoload.php';

$app = require_once __DIR__.'/bootstrap/app.php';
$kernel = new \App\Console\Kernel($argv);
