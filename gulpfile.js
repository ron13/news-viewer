var gulp = require('gulp'),
    less = require('gulp-less'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require("gulp-concat");;

function swallowError (error) {
    console.log(error.toString())
    this.emit('end')
}

gulp.task('less', function () {

    return gulp
        .src('resources/less/style.less')
        .pipe(less())
        .on('error', swallowError)
        .pipe(cssnano())
        .pipe(rename({ suffix: '.min' }))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css'));
})

gulp.task('scripts', function () {
    return gulp
        .src([
            'node_modules/jquery/dist/jquery.js',
            'resources/js/**/**.js'
        ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .on('error', swallowError)
        .pipe(gulp.dest('public/js'))
});

gulp.task('watch', ['less', 'scripts'], function () {
    gulp.watch('resources/less/**/*.less', ['less']);
    gulp.watch('resources/js/**/**.js', ['scripts']);
});