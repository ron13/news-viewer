<?php
/**
 * Date: 10/12/2018
 * Time: 8:40 PM
 */
namespace Main;

use App\Exceptions\Handler;
use Dotenv\Dotenv;
use Whoops\Run;
use Whoops\Util\Misc;

class App
{

    private static $instance;

    private function __construct()
    {
        // Load configs
        $dotenv = new Dotenv(self::path('/'));
        $dotenv->load();


        if (!Misc::isCommandLine()) {

            // Register error handler
            $whoops = new Run();
            $whoops->pushHandler(new Handler());
            $whoops->register();

            // Process request
            $this->processRequest();
        }
    }

    private function __clone()
    {
        return self::getInstance();
    }

    /**
     * Retrieve App instance
     *
     * @return App
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Find and call action binded to requested route
     */
    private function processRequest()
    {
        Router::callAction($_SERVER['REQUEST_URI']);
    }

    /**
     * Retrieve application path relative to root dir
     *
     * @param string $path
     * @return string
     */
    public static function path($path = '')
    {
        return __DIR__ . "/..{$path}";
    }

}
