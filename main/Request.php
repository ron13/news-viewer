<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 10/13/2018
 * Time: 9:55 PM
 */

namespace Main;


class Request
{

    private static $instance;

    private $method;
    private $params = [];
    private $headers = [];

    private function __construct()
    {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->params = $_REQUEST;
        $this->setRequestHeaders();
    }

    private function __clone()
    {
        return self::getInstance();
    }

    /**
     * Retrieve Route singleton instance
     *
     * @return Request
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __get($name)
    {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        }
    }

    /**
     * Retrieve request method type
     *
     * @return string
     */
    public function method()
    {
        return $this->method;
    }

    /**
     * Retrieve request params
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        return isset($this->params[$key]) ? $this->params[$key] : $default;
    }

    /**
     * Retrieve request headers
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function header($key, $default = null)
    {
        return isset($this->headers[$key]) ? $this->headers[$key] : $default;
    }

    /**
     * Add new parameter to request
     *
     * @param string $key
     * @param $value
     */
    public function add(string $key, $value)
    {
        $this->params[$key] = $value;
    }

    /**
     * Grab headers from request
     */
    private function setRequestHeaders() {
        $this->headers = [];
        foreach($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $this->headers[$header] = $value;
        }
    }
}