<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 10/13/2018
 * Time: 10:59 PM
 */

namespace Main;


class Response
{
    const HTTP_CONTINUE                        = 100;
    const HTTP_SWITCHING_PROTOCOLS             = 101;
    const HTTP_PROCESSING                      = 102;            // RFC2518
    const HTTP_EARLY_HINTS                     = 103;            // RFC8297
    const HTTP_OK                              = 200;
    const HTTP_CREATED                         = 201;
    const HTTP_ACCEPTED                        = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION   = 203;
    const HTTP_NO_CONTENT                      = 204;
    const HTTP_RESET_CONTENT                   = 205;
    const HTTP_PARTIAL_CONTENT                 = 206;
    const HTTP_MULTI_STATUS                    = 207;            // RFC4918
    const HTTP_ALREADY_REPORTED                = 208;            // RFC5842
    const HTTP_IM_USED                         = 226;            // RFC3229
    const HTTP_MULTIPLE_CHOICES                = 300;
    const HTTP_MOVED_PERMANENTLY               = 301;
    const HTTP_FOUND                           = 302;
    const HTTP_SEE_OTHER                       = 303;
    const HTTP_NOT_MODIFIED                    = 304;
    const HTTP_USE_PROXY                       = 305;
    const HTTP_RESERVED                        = 306;
    const HTTP_TEMPORARY_REDIRECT              = 307;
    const HTTP_PERMANENTLY_REDIRECT            = 308;            // RFC7238
    const HTTP_BAD_REQUEST                     = 400;
    const HTTP_UNAUTHORIZED                    = 401;
    const HTTP_PAYMENT_REQUIRED                = 402;
    const HTTP_FORBIDDEN                       = 403;
    const HTTP_NOT_FOUND                       = 404;
    const HTTP_METHOD_NOT_ALLOWED              = 405;
    const HTTP_NOT_ACCEPTABLE                  = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED   = 407;
    const HTTP_REQUEST_TIMEOUT                 = 408;
    const HTTP_CONFLICT                        = 409;
    const HTTP_GONE                            = 410;
    const HTTP_LENGTH_REQUIRED                 = 411;
    const HTTP_PRECONDITION_FAILED             = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE        = 413;
    const HTTP_REQUEST_URI_TOO_LONG            = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE          = 415;
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    const HTTP_EXPECTATION_FAILED              = 417;
    const HTTP_I_AM_A_TEAPOT                   = 418;             // RFC2324
    const HTTP_MISDIRECTED_REQUEST             = 421;             // RFC7540
    const HTTP_UNPROCESSABLE_ENTITY            = 422;             // RFC4918
    const HTTP_LOCKED                          = 423;             // RFC4918
    const HTTP_FAILED_DEPENDENCY               = 424;             // RFC4918

    const CONTENT_JSON                         = 'application/json';
    const CONTENT_HTML                         = 'text/html';
    const CONTENT_EVENT_STREAM                 = 'text/event-stream';

    private $code;
    private $contentType;
    private $data;

    private static $instance;

    private function __construct()
    {
        $this->code        = self::HTTP_OK;
        $this->contentType = self::CONTENT_HTML;
        $this->data        = '';
    }

    private function __clone()
    {
        return self::getInstance();
    }

    /**
     * Retrieve Route singleton instance
     *
     * @return Response
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Send response output
     */
    public function send()
    {
        http_response_code($this->code);
        header("Content-Type: {$this->contentType}");
        echo $this->data;
    }

    /**
     * Set HTTP response code
     *
     * @param int $responseCode
     * @return $this
     */
    public function setCode(int $responseCode)
    {
        $this->code = $responseCode;
        return $this;
    }

    /**
     * Set content type to JSON and load json-data
     *
     * @param $data
     * @param int $responseCode
     * @return $this
     */
    public function json($data, $responseCode = self::HTTP_OK)
    {
        $this->code        = $responseCode;
        $this->contentType = self::CONTENT_JSON;
        $this->data        = json_encode($data);
        return $this;
    }

    /**
     * Set content type to HTML and load view
     *
     * @param string $view
     * @param array $data
     * @param int $responseCode
     * @return $this
     */
    public function view(string $view, array $data = [], $responseCode = self::HTTP_OK)
    {
        $view = App::path("/resources/views/{$view}.php");
        if (file_exists($view)) {
            ob_start();
            extract($data);
            require $view;
            $html = ob_get_clean();
            $this->code        = $responseCode;
            $this->contentType = self::CONTENT_HTML;
            $this->data        = $html;
        } else {
            $this->setCode(Response::HTTP_NOT_FOUND);
        }
        return $this;
    }
}