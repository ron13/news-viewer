<?php
/**
 * Date: 10/13/2018
 * Time: 9:18 PM
 */

namespace Main;


class Router
{
    const METHOD_POST = 'post';
    const METHOD_GET = 'get';

    private static $instance;

    private $routes = [];

    private function __construct()
    {
        $this->routes = [
            self::METHOD_GET => [],
            self::METHOD_POST => [],
        ];
    }

    private function __clone()
    {
        return self::getInstance();
    }

    /**
     * Retrieve Route singleton instance
     *
     * @return Router
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function __callStatic($name, $arguments)
    {
        $router = self::getInstance();
        if (method_exists($router, $name)) {
            call_user_func_array([$router, $name], $arguments);
        }
    }

    /**
     * Add GET route
     *
     * @param string $route
     * @param string|callable $action
     * @return $this
     */
    public static function get(string $route, $action)
    {
        $router = self::getInstance();
        return $router->addRoute(self::METHOD_GET, $route, $action);
    }

    /**
     * Add POST route
     *
     * @param string $route
     * @param string|callable $action
     * @return $this
     */
    public static function post(string $route, $action)
    {
        $router = self::getInstance();
        return $router->addRoute(self::METHOD_POST, $route, $action);
    }

    /**
     * Add new route
     *
     * @param string $method
     * @param string $route
     * @param string|callable $action
     * @return $this
     */
    public function addRoute(string $method, string $route, $action)
    {
        $this->routes[$method][$route] = $action;
        return $this;
    }

    /**
     * Find action for requested URL
     *
     * @param string $url
     * @return callable|bool
     */
    private function findAction(string $url)
    {
        foreach ($this->routes as $method => $routes) {
            foreach ($routes as $route => $action) {
                if ($this->matchRoute($url, $route) && $method == request()->method()) {
                    return $action;
                }
            }
        }
        return false;
    }

    /**
     * Retrieve arguments for callable route action
     *
     * @param string $url
     * @return array|bool
     */
    private function getRouteArguments(string $url)
    {
        foreach ($this->routes as $method => $routes) {
            foreach ($routes as $route => $action) {
                if ($this->matchRoute($url, $route) && $method == request()->method()) {
                    $arguments = $this->matchRouteArguments($url, $route);
                    $arguments[] = Request::getInstance();
                    return $arguments;
                }
            }
        }
        return false;
    }

    /**
     * Call action for route
     *
     * @param string $url
     */
    public static function callAction(string $url)
    {
        $router = self::getInstance();
        $action = $router->findAction($url);
        if ($action) {
            $arguments = $router->getRouteArguments($url);
            if (is_callable($action)) {
                call_user_func_array($action, $arguments);
            } elseif (is_string($action)) {
                list($controller, $method) = explode('@', $action);
                if (class_exists($controller) && method_exists($controller, $method)) {
                    $controller = new $controller();
                    $response = call_user_func_array([$controller, $method], $arguments);
                    $response->send();
                    return;
                }
            }
        }
        Response::getInstance()
            ->setCode(Response::HTTP_METHOD_NOT_ALLOWED)
            ->send();
    }

    /**
     * Is URL matching selected route
     *
     * @param $url
     * @param $pattern
     * @return bool
     */
    private function matchRoute($url, $pattern)
    {
        // get parts of the url
        $urlParts     = array_filter(explode('/', $url));
        $patternParts = array_filter(explode('/', $pattern));
        // match if number of parts are equal
        if (count($urlParts) != count($patternParts)) {
            return false;
        }

        // preg match in a loop
        foreach ($urlParts as $i => $urlPart) {
            if(preg_match('#\{(.*?)\}#', $patternParts[$i])) {
                continue;
            }
            if(! preg_match('/' . $patternParts[$i] .'/', $urlPart)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieve URL params for route
     *
     * @param $url
     * @param $pattern
     * @return array|bool
     */
    private function matchRouteArguments($url, $pattern)
    {
        // get parts of the url
        $urlParts     = array_filter(explode('/', $url));
        $patternParts = array_filter(explode('/', $pattern));
        $params       = [];

        // preg match in a loop
        foreach ($urlParts as $i => $urlPart) {
            if(preg_match('#\{(.*?)\}#', $patternParts[$i])) {
                $params[] = $urlPart;
                continue;
            }
            if(! preg_match('/' . $patternParts[$i] .'/', $urlPart)) {
                return false;
            }
        }
        return $params;
    }

    /**
     * Retrieve url with path
     *
     * @param string $path
     * @return string
     */
    function url($path = ''){
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['HTTP_HOST'],
            $path
        );
    }
}