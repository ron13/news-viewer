<?php
if (! function_exists('config')) {
    /**
     * Retrieve config value
     *
     * @param string $path
     * @param null $default
     * @return null
     */
    function config(string $path, $default = null)
    {
        //    var_dump(strpos($path, '.'));
        if (strpos($path, '.') !== false) {
            list($file, $key) = explode('.', $path);
            if (file_exists($configFile = \Main\App::path("/config/{$file}.php"))) {
                $values = include $configFile;
                return isset($values[$key]) ? $values[$key] : $default;
            }
        }
        return $default;
    }
}

if (! function_exists('app')) {
    /**
     * Retrieve App instance
     *
     * @return \Main\App
     */
    function app()
    {
        return \Main\App::getInstance();
    }
}

if (! function_exists('router')) {
    /**
     * Retrieve Router instance
     *
     * @return \Main\Router
     */
    function router()
    {
        return \Main\Router::getInstance();
    }
}

if (! function_exists('response')) {
    /**
     * Retrieve Response instance
     *
     * @return \Main\Response
     */
    function response()
    {
        return \Main\Response::getInstance();
    }
}

if (! function_exists('request')) {
    /**
     * Retrieve Request instance
     *
     * @return \Main\Request
     */
    function request()
    {
        return \Main\Request::getInstance();
    }
}

if (! function_exists('__')) {
    /**
     * Localize text.
     *
     * @param $text
     * @return mixed
     */
    function __($text, $params = [])
    {
        // TODO: string localization
        return $text;
    }
}

if (! function_exists('env')) {
    /**
     * Get the value of the environment variable.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (($valueLength = strlen($value)) > 1 && $value[0] === '"' && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (! function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (! function_exists('dd')) {
    /**
     * Dump and die.
     *
     * @return mixed
     */
    function dd()
    {
        array_map(function($x) {
            var_dump($x);
        }, func_get_args());
        die;
    }
}

if (! function_exists('loadView')) {
    /**
     * Load template.
     *
     * @param string $view
     * @param array $data
     * @return mixed
     */
    function loadView(string $view, array $data = [])
    {
        $view = \Main\App::path("/resources/views/{$view}.php");
        if (file_exists($view)) {
            extract($data);
            include $view;
        }
    }
}