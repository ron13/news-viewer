(function ($) {
    if (typeof appSettings == 'undefined') return;
    var conn = new WebSocket(appSettings.socketAddress);
    conn.onmessage = function(e) {
        var data = JSON.parse(e.data);
        var $item = $('<li class="news-feed__item news-feed__item--' + data.id + '" id="news-item-' + data.id + '">' +
            '<h2 class="news-feed__item-title">' + data.message + '</h2>' +
            '<p class="news-feed__item-date">' + data.date + '</p>' +
            '</li>');
        $('.news-feed').prepend($item);
        if ($('.news-feed__item').length > appSettings.maxItems) {
            $('.news-feed__item:nth-child(n+' + (appSettings.maxItems+1) + ')').remove();
        }
    };
})(jQuery);