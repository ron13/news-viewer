        </div>
    </div>

    <script>
        var appSettings = {
            socketAddress: "<?php  echo 'ws://' . request()->header('Host') . ':' . config('news.port') ?>",
            maxItems: <?php echo config('news.max')?>
        };
    </script>
    <script src="<?php echo router()->url('/js/main.min.js') ?>"></script>
</body>
</html>