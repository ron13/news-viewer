<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo config('app.name') ?></title>
    <link rel="stylesheet" href="<?php echo router()->url('/css/style.min.css') ?>">
</head>
<body>
    <div class="app">
        <div class="app__width">
