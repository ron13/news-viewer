<?php loadView('header') ?>

            <h1 class="app__title"><?php echo __('News feed') ?></h1>
            <ol class="news-feed">
                <?php foreach ($news as $item): ?>
                    <li class="news-feed__item news-feed__item--<?php echo $item['id'] ?>" id="news-item-<?php echo $item['id'] ?>">
                        <h2 class="news-feed__item-title"><?php echo $item['message'] ?></h2>
                        <p class="news-feed__item-date"><?php echo $item['date'] ?></p>
                    </li>
                <?php endforeach ?>
            </ol>

<?php loadView('footer') ?>
